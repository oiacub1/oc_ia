package main

import (
	db "cyzachallenge/db"
	"testing"
)

func TestSignIn(t *testing.T) {
	_, err := db.GetUser("xxxx", "xxxxx", false)
	if err == nil {
		t.Errorf("Faield login. Expected false and get true")
	}
}

func TestCheckExistingUser(t *testing.T) {
	exist := db.CheckExistingUser("xxxx")
	if exist {
		t.Errorf("Faield checking user exist. Expected false and get true")
	}
}
