package main

import (
	db "cyzachallenge/db"
	models "cyzachallenge/models"
	utils "cyzachallenge/utils"
	"errors"
	"html/template"
	"log"
	"net/http"
	"strings"
)

func main() {
	handleRequests()
}

func handleRequests() {
	http.HandleFunc("/", SendLoginPage)
	http.HandleFunc("/login", HomePage)
	http.HandleFunc("/userHome", UserHome)
	http.HandleFunc("/dologin", LoginUser)
	http.HandleFunc("/signUp", SignUp)
	http.HandleFunc("/profile", Profile)
	http.HandleFunc("/logout", Logout)
	http.HandleFunc("/register", Register)
	http.HandleFunc("/saveProfile", SaveProfile)
	http.HandleFunc("/auth/google", utils.HandleGoogleLogin)
	http.HandleFunc("/auth/googleExisting", utils.HandleGoogleLoginExisting)
	http.HandleFunc("/auth/google/callback", utils.CallBackFromGoogle)
	http.HandleFunc("/auth/google/callbackExisting", utils.CallBackFromGoogleExisting)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func LoginUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Fatal(err)
	}
	user := models.User{
		Fullname:     "",
		Telephone:    "",
		Email:        r.Form.Get("femail"),
		Password:     r.Form.Get("fpassword"),
		IsGoogleUser: false,
	}
	user, err = db.GetUser(user.Email, user.Password, user.IsGoogleUser)
	if err != nil {
		HomePageWithError(w, r, err)
	} else {
		utils.GenerateCookie(w, user.Email)
		Profile(w, r)
	}
}

func Profile(w http.ResponseWriter, r *http.Request) {
	if utils.CheckCookie(w, r) {
		err := r.ParseForm()
		if err != nil {
			log.Fatal(err)
		}
		t, _ := template.ParseFiles("./static/profile.html") // Parse template file.
		user := db.GetUserByToken(utils.GetToken(r))
		t.Execute(w, user)
	} else {
		http.Redirect(w, r, "../", http.StatusTemporaryRedirect)
	}
}

func UserHome(w http.ResponseWriter, r *http.Request) {
	if utils.CheckCookie(w, r) {
		user := db.GetUserByToken(utils.GetToken(r))
		t, _ := template.ParseFiles("./static/userhome.html") // Parse template file.
		t.Execute(w, user)
	} else {
		http.Redirect(w, r, "../", http.StatusTemporaryRedirect)
	}
}

func Register(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Fatal(err)
	}
	if !db.CheckExistingUser(r.Form.Get("femail")) {
		user := models.User{
			Email: r.Form.Get("femail"), Password: r.Form.Get("fpassword")}
		db.CreateUser(user)
		utils.GenerateCookie(w, user.Email)
		http.Redirect(w, r, "../profile", http.StatusTemporaryRedirect)
	} else {
		t, _ := template.ParseFiles("./static/signup.html") // Parse template file.
		t.Execute(w, errors.New("Existing user"))
	}
}

func SaveProfile(w http.ResponseWriter, r *http.Request) {
	if utils.CheckCookie(w, r) {
		err := r.ParseForm()
		if err != nil {
			log.Fatal(err)
		}
		user := db.GetUserByToken(utils.GetToken(r))
		oldemail := user.Email
		user.Fullname = r.Form.Get("fname")
		if !user.IsGoogleUser {
			user.Email = r.Form.Get("femail")
		}
		user.Telephone = r.Form.Get("ftelephone")
		utils.GenerateCookie(w, user.Email)
		db.UpdateUser(oldemail, user)
		http.Redirect(w, r, "../userHome", http.StatusTemporaryRedirect)
	} else {
		http.Redirect(w, r, "../", http.StatusTemporaryRedirect)
	}
}

func SignUp(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("./static/signup.html") // Parse template file.
	t.Execute(w, nil)
}

func HomePage(w http.ResponseWriter, r *http.Request) {
	if !utils.CheckCookie(w, r) {
		t, _ := template.ParseFiles("./static/index.html") // Parse template file.
		if r.URL.RawQuery != "" {
			var err = strings.Replace(string(r.URL.RawQuery), "_", " ", -1)
			t.Execute(w, errors.New(err))
		} else {
			t.Execute(w, nil)
		}
	} else {
		http.Redirect(w, r, "/userHome", http.StatusSeeOther)
	}
}

func SendLoginPage(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func HomePageWithError(w http.ResponseWriter, r *http.Request, err error) {
	t, _ := template.ParseFiles("./static/index.html") // Parse template file.
	t.Execute(w, err)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	c := http.Cookie{
		Name:   "token",
		MaxAge: -1}
	http.SetCookie(w, &c)
	db.DeleteToken(utils.GetToken(r))
	SendLoginPage(w, r)
}
