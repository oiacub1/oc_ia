package cyzachallenge

import (
	models "cyzachallenge/models"
	"database/sql"
	"errors"
	"log"

	"github.com/go-sql-driver/mysql"
)

const (
	dburl = "root:calipso@tcp(127.0.0.1:3306)/models"
)

func CheckExistingUser(email string) bool {
	db := dbConn()

	res, _ := db.Query("SELECT * FROM users where email = ?", email)

	// defer the close till after the main function has finished
	// executings
	defer db.Close()

	if res.Next() {
		return true
	}
	return false
}

func GetUser(username string, password string, google bool) (models.User, error) {
	db := dbConn()
	var u models.User
	err := db.QueryRow("select email, telephone, fullname, password, isgoogleuser from users WHERE email = ?", username).Scan(&u.Email, &u.Telephone, &u.Fullname, &u.Password, &u.IsGoogleUser)
	// defer the close till after the main function has finished
	// executings
	defer db.Close()

	if err == nil {
		if u.IsGoogleUser {
			return u, nil
		} else {
			if u.Password == password {
				return u, nil
			} else {
				return models.User{}, errors.New("password is incorrect")
			}
		}

	} else {
		return models.User{}, errors.New("username entered does not exist")
	}
}

func UserExist(username string, password string, google bool) bool {
	_, err := GetUser(username, password, google)
	if err != nil {
		return false
	} else {
		return true
	}
}

func UpdateUser(email string, user models.User) {
	db := dbConn()
	db.Exec("UPDATE users set email=?, telephone=?, fullname=?, password=?, isgoogleuser=? where email=?", user.Email, user.Telephone, user.Fullname, user.Password, user.IsGoogleUser, email)
	// defer the close till after the main function has finished
	// executings
	defer db.Close()
}

func CreateUser(user models.User) {
	db := dbConn()
	db.Exec("INSERT INTO users(email, telephone, fullname, password, isgoogleuser) VALUES (?, ?, ?, ?, ?)", user.Email, user.Telephone, user.Fullname, user.Password, user.IsGoogleUser)
	// defer the close till after the main function has finished
	// executings
	defer db.Close()
}

func SaveToken(token string, userMail string) {
	db := dbConn()
	db.Exec("INSERT INTO tokens(token, username) VALUES (?, ?)", token, userMail)
	// defer the close till after the main function has finished
	// executings
	defer db.Close()
}

func CheckToken(token string) bool {
	db := dbConn()

	res, _ := db.Query("SELECT count(*) FROM tokens where token = ?", token)

	// defer the close till after the main function has finished
	// executings
	defer db.Close()

	if res.Next() {
		return true
	}
	return false
}

func GetUserByToken(token string) models.User {
	db := dbConn()
	var u models.User
	db.QueryRow("select email, telephone, fullname, password, isgoogleuser from users WHERE email=(select username from tokens where token=?)", token).Scan(&u.Email, &u.Telephone, &u.Fullname, &u.Password, &u.IsGoogleUser)
	// defer the close till after the main function has finished
	// executings
	defer db.Close()
	return u
}

func DeleteToken(token string) {
	db := dbConn()
	db.Exec("DELETE FROM tokens where token=?", token)
	// defer the close till after the main function has finished
	// executings
	defer db.Close()
}

func dbConn() (db *sql.DB) {
	cfg := mysql.Config{
		User:   "calipso",
		Passwd: "calipso",
		Net:    "tcp",
		Addr:   "127.0.0.1:3306",
		DBName: "model",
	}
	// Get a database handle.
	var err error
	db, err = sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}
	pingErr := db.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}
	return db
}
