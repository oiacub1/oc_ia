module cyzachallenge

go 1.18

require golang.org/x/oauth2 v0.0.0-20220411215720-9780585627b5

require (
	cloud.google.com/go v0.65.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/uuid v1.3.0
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
