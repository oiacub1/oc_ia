package cyzachallenge

type User struct {
	Fullname     string
	Telephone    string
	Email        string
	Password     string
	IsGoogleUser bool
}
