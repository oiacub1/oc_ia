package cyzachallenge

import (
	db "cyzachallenge/db"
	models "cyzachallenge/models"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	oauthConfGl = &oauth2.Config{
		ClientID:     "944431779596-a1pm3cjaa9ntpckiqocqu87onq12aovj.apps.googleusercontent.com",
		ClientSecret: "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",
		RedirectURL:  "http://testingdevelopment.calipso.work:8080/auth/google/callback",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	oauthStateStringGl = ""
)

type GoogleUser struct {
	Id           string `json:"id"`
	Email        string `json:"email"`
	VerfiedEmail bool   `json:"verified_email"`
	Name         string `json:"name"`
	GivenName    string `json:"given_name"`
	FamilyName   string `json:"family_name"`
	Picture      string `json:"picture"`
	Hd           string `json:"hd"`
}

/*
HandleGoogleLogin Function
*/

func HandleGoogleLoginExisting(w http.ResponseWriter, r *http.Request) {
	oauthConfGl.RedirectURL = "http://testingdevelopment.calipso.work:8080/auth/google/callbackExisting"
	HandleLogin(w, r, oauthConfGl, oauthStateStringGl)
}

func HandleGoogleLogin(w http.ResponseWriter, r *http.Request) {
	oauthConfGl.RedirectURL = "http://testingdevelopment.calipso.work:8080/auth/google/callback"
	HandleLogin(w, r, oauthConfGl, oauthStateStringGl)
}

/*
CallBackFromGoogle Function
*/
func CallBackFromGoogle(w http.ResponseWriter, r *http.Request) {
	state := r.FormValue("state")
	if state != oauthStateStringGl {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")

	if code == "" {
		w.Write([]byte("Code Not Found to provide AccessToken..\n"))
		reason := r.FormValue("error_reason")
		if reason == "user_denied" {
			w.Write([]byte("User has denied Permission.."))
		}
		// User has denied access..
		// http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	} else {
		token, err := oauthConfGl.Exchange(oauth2.NoContext, code)
		if err != nil {
			return
		}

		resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + url.QueryEscape(token.AccessToken))
		if err != nil {
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		defer resp.Body.Close()

		response, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		var googleUser GoogleUser
		if err := json.Unmarshal(response, &googleUser); err != nil {
			panic(err)
		}
		user := models.User{
			Fullname:     googleUser.Name,
			Email:        googleUser.Email,
			IsGoogleUser: true,
		}
		db.CreateUser(user)
		GenerateCookie(w, googleUser.Email)
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
	}
}

func CallBackFromGoogleExisting(w http.ResponseWriter, r *http.Request) {
	state := r.FormValue("state")
	if state != oauthStateStringGl {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")

	if code == "" {
		w.Write([]byte("Code Not Found to provide AccessToken..\n"))
		reason := r.FormValue("error_reason")
		if reason == "user_denied" {
			w.Write([]byte("User has denied Permission.."))
		}
		// User has denied access..
		// http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	} else {
		token, err := oauthConfGl.Exchange(oauth2.NoContext, code)
		if err != nil {
			return
		}

		resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + url.QueryEscape(token.AccessToken))
		if err != nil {
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		defer resp.Body.Close()

		response, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		var googleUser GoogleUser
		if err := json.Unmarshal(response, &googleUser); err != nil {
			panic(err)
		}
		if db.CheckExistingUser(googleUser.Email) {
			GenerateCookie(w, googleUser.Email)
			http.Redirect(w, r, "/profile", http.StatusSeeOther)
		} else {
			http.Redirect(w, r, "/login?username_entered_does_not_exist", http.StatusSeeOther)
		}
	}
}

func HandleLogin(w http.ResponseWriter, r *http.Request, oauthConf *oauth2.Config, oauthStateString string) {
	URL, _ := url.Parse(oauthConf.Endpoint.AuthURL)
	parameters := url.Values{}
	parameters.Add("client_id", oauthConf.ClientID)
	parameters.Add("scope", strings.Join(oauthConf.Scopes, " "))
	parameters.Add("redirect_uri", oauthConf.RedirectURL)
	parameters.Add("response_type", "code")
	parameters.Add("state", oauthStateString)
	URL.RawQuery = parameters.Encode()
	url := URL.String()
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}
