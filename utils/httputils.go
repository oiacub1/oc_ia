package cyzachallenge

import (
	db "cyzachallenge/db"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
)

func CheckCookie(w http.ResponseWriter, r *http.Request) bool {
	err := r.ParseForm()
	tokenCookie, err := r.Cookie("token")
	if err != nil {
		return false
	}
	if !db.CheckToken(tokenCookie.Value) {
		return false
	}
	return true
}

func GetToken(r *http.Request) string {
	tokenCookie, _ := r.Cookie("token")
	return tokenCookie.Value
}

func GenerateCookie(w http.ResponseWriter, userId string) {

	expires := time.Now().AddDate(1, 0, 0)
	ck := http.Cookie{
		Name:    "token",
		Domain:  "localhost:8080",
		Path:    "/",
		Expires: expires,
	}
	uuidWithHyphen := uuid.New()
	uuid := strings.Replace(uuidWithHyphen.String(), "-", "", -1)
	// value of cookie
	ck.Value = uuid
	db.SaveToken(uuid, userId)
	// write the cookie to response
	http.SetCookie(w, &ck)
}
